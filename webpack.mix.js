let mix = require('laravel-mix');
require('dotenv').config();
let webpack = require('webpack');
const HOTEL_VERSION = process.env.HOTEL_VERSION;
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.webpackConfig({
   plugins:[
      new webpack.DefinePlugin({
         VERSION: JSON.stringify(HOTEL_VERSION), 
      })
   ]
})

mix.react('resources/assets/js/app1.js', 'public/js/app1.js')
   .sass('resources/assets/sass/app.scss', 'public/css');

// mix.react('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css');

// mix.react('resources/assets/js/app.js', 'public/js')
//    .sass('resources/assets/sass/app.scss', 'public/css')
//    .js('resources/assets/js/index.js', 'public/js/main.js')
//    .babel('public/js/main.js', 'public/js/main.js');

