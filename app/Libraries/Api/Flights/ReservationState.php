<?php

namespace App\Http\Libraries\Api\Flights;

/**
 * Singleton with internal state
 * This is for laravel to keep check of how many payment auth attempts has been made
 * interpret ltr-platfrom response and set a redirect url.
 */
class ReservationState
{
    private static $instance;

    private $data = [
        'bookingAttempt' => 0,
        'log' => [],
    ];

    private $solutions = [
        // ltr-platform's status code => solution
        200 => [
            'description' => 'Booking Success',
            'redirectUrl' => '/flights/thank-you',
        ],
        500 => [
            'description' => 'Max payment auth attempt reached',
            'callback' => 'isMaxAuthAttemptReached',
            'params' => [
                'maxAttempts' => 5,
                'is' => [
                    'redirectUrl' => '/flights/error',
                ],
                'not' => [
                    'redirectUrl' => '/flights/payment/{orderId}',
                ],
            ],
            'redirectUrl' => 'TBD',
        ],
    ];

    private $solutionDefault = [
        'status' => null,
        'message' => 'ltr-platform did not return status',
    ];

    /**
     * Singleton. If $instance exists, use and return it. Else create a new self.
     */
    private static function create()
    {
        if(!self::$instance){
            self::$instance = new self();
        }
        return self::$instance;
    }
    
    /**
     * Undocumented function
     *
     * @param array $request
     * @param array $response
     * @return array
     */
    public static function resolve(array $request, array $response): array
    {
        $inst = self::create();
        $data = session()->get('flights.ReservationState.data');
        if($data){
            // on first load, there is no data in session. Afterward, use the session data array
            $inst->data = $data;
        }
        $inst->data['bookingAttempt']++; //considered booking attempt made
        $solution = $inst->getSolutionByResponse($response);
        $inst->addLog($request, $response, $solution);
        // Important, put data back in session for next page load
        session()->put('flights.ReservationState.data',  $inst->data);
        return $solution;
    }

    /**
     * 
     *
     * @param array $request
     * @param array $response
     * @param array $solution
     * @return void
     */
    private static function addLog(array $request, array $response, array $solution)
    {
        $inst = self::create();
        $inst->data['log'][] = [
            'request' => $request,
            'response' => $response,
            'solution' => [
                'redirectUrl' => $solution['redirectUrl']
            ],
        ];
    }

    /**
     * Undocumented function
     *
     * @param array $response
     * @return array
     */
    private static function getSolutionByResponse(array $response): array
    {
        $inst = self::create();
      
        // Get the rule by status code
        $solution = $inst->getSolutionByStatus(array_get($response, 'status'));
           
        // Run the rule
        if ($solution && isset($solution['callback'])) {
            // Code for if we want to check callable. Not needed now as everything is set internally
            //var_dump(is_callable([__CLASS__, $callback], true, $callable_name));
            $callback = $solution['callback'];
            $result = $inst::$callback($solution);
        } elseif($solution) {
            $result = $solution; // no callback, nothing to do, return rule
        }
        return $result ?? $inst->solutionDefault;
    }

    /**
     * Undocumented function
     *
     * @param integer $status
     * @return array|null
     */
    public static function getSolutionByStatus(int $status = null): ?array
    {
        $inst = self::create();
        return  array_get($inst->solutions, $status);
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public static function get()
    {
        return self::create()->data;
    }

    // All callbacks go here. Add more as needed.

    /**
     * Undocumented function
     *
     * @param array $rule
     * @return array
     */
    private static function isMaxAuthAttemptReached(array $solution): array
    {
        $inst = self::create();
        echo 'now comparing ' . $inst->data['bookingAttempt'] . ' to ' . $solution['params']['maxAttempts'];
        $isOrNot = 'not'; // assume max not yet reached
        if ($inst->data['bookingAttempt'] > $solution['params']['maxAttempts']) {
            $isOrNot = 'is'; // get is.redirectUrl
        } 
        $solution['redirectUrl'] = $solution['params'][$isOrNot]['redirectUrl'];
        $solution['currentAttempt'] = $inst->data['bookingAttempt'];
        return $solution;
    }
}
