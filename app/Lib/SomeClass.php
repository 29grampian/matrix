<?php

namespace App\Lib;

use App\Lib\SomeInterface;

class SomeClass implements SomeInterface{
	public function do(){
		echo __FILE__.__FUNCTION__;
	}
}