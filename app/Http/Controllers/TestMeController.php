<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Libraries\ArrayHelper;
use ArrayAccess;

class TestMeController extends Controller
{
    public $flights = [
        'travelers' => [
            [
                'name'=>'john',
                'seats' => [
                    'price' => 2,
                ]
            ],
            [
                'name'=>'jane',
                'seats' => [
                    'price' => 1,
                ]
            ]
        ]

    ];
    public function getIndex(){
        return view(
            'test'
        );
    }
    
    public function postIndex(Request $request){
       print_r($request->all());
        // return ('thankyou', []);
    }

    public function getTravelersInfoByKey($travelers, $key){
        return ArrayHelper::pluck($travelers, $key);

    }
}
