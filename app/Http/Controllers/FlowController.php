<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class FlowController extends Controller
{
    
    public function getIndex(){
        return view(
            'flow.index'
        );
    }
    
    public function postIndex(){
       return redirect('/flow');
    }
   
    public function postProductSearch(Request $request, $product){
       
       $p = $request->get('queueSafe');
       $parts = explode(',', $p);
       Session::put('queue', $parts);
       Session::save();
       return redirect ('/'.$product);
    }
    public function getProductSearch(Request $request, $product){
        
        $q = Session::get('queue',[]);
        

        $url = '/' . $product .'/finish';
        $q = implode(',',$q);
       
         return view(
             'flow.search-results',
             [
                 'product' => $product . ' search results page',
                 'queue' => $q,
                 'url' => $url,
                 'cart' => Session::get('cart',[]),
                 'cartStr' => implode(',',Session::get('cart',[]))
             ]
         );
     }    

   

    public function postFinish(Request $request, $product){
        $q = Session::get('queue',[]);
        $cart = Session::get('cart');
       
        if(!$cart){
            $cart = [];
        }
        $cart[] = $product;
        Session::put('cart', $cart);
        
        Session::save();
      
        if(count($q)>0){
            $first = $q[0];
            $url = '/' . $first;
            
            array_shift($q);
         
            Session::put('queue', $q);
            Session::save();
            return redirect($url);
        }else{
            print_r($cart);
            return 'p or i';
        }
      
    }
}
