<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use WW\Flights\flights as Flights;
use WW\Cars\cars as Cars;
use WW\PNR_Remarks\pnr_remarks as PNR;
use WW\Gundam\gundam as Gundam;
use WW\Gundam\Models\models as GundamModels;
use App\Libraries\Store;
use Session;

class MyController extends Controller
{
    private $lookupResults = [];
    private $uniqueCarriers = [];

    private $baseGrid = [
        'colHeader' => [],
        'rowHeader' => [
            [
                'label' => 'Nonstop',
                'field' => 'stops',
                'queryParams' => [
                    'value' => 0,
                ],
            ],
            [
                'label' => '1 Stop',
                'field' => 'stops',
                'queryParams' => [
                    'value' => 1,
                ],
            ],
            [
                'label' => '2 Stops',
                'field' => 'stops',
                'queryParams' => [
                    'value' => 2,
                ],
            ]
        ],
        'matrix' => [],
    ];

    private $baseGrid2 = [
        [
            [
                'type' => 'colHeader',
            ]

        ],
        [
            [
                'label' => 'Nonstop',
                'type' => 'rowHeader',
                'queryParams' => [
                    'stop' => 0,
                ]
            ]

        ],
        [
            [
                'label' => '1 Stop',
                'type' => 'rowHeader',
                'queryParams' => [
                    'stop' => 1,
                ]
            ]

        ],
        [
            [
                'label' => '2 Stops',
                'type' => 'rowHeader',
                'queryParams' => [
                    'stop' => 2,
                ]
            ]

        ],

    ] ;

    private $x = [
            ['name'=>'mu 0 stop $1', 'seg'=>[1], 'carrier'=>'mu', 'price'=>1],
            ['name'=>'ca 1 stop $1', 'seg'=>[1,2], 'carrier'=>'ca', 'price'=>1],
            ['name'=>'ua 2 stops $100', 'seg'=>[1,2,3], 'carrier'=>'ua', 'price'=>100],
            ['name'=>'ua 2 stops $1', 'seg'=>[1,2,3], 'carrier'=>'ua', 'price'=>2],
            ['name'=>'mu 2 stops $1', 'seg'=>[1,2,3], 'carrier'=>'mu', 'price'=>2],
            ['name'=> 'mu 2 stops $100', 'seg'=>[1,2,3], 'carrier'=>'mu','price'=>100],
            ['name'=>'ca 3 stop $1', 'seg'=>[1,2,3,4], 'carrier'=>'ca', 'price'=>10000],
        ];


    public function getUniqueCarriers(array $x): array
    {
        return array_unique(array_column($x, 'carrier'));
    }

    public function getItemByStopCarrier(int $stop = null, string $carrier = null): array{
        $item = [];
        if(isset($this->groups[$stop]) && $this->groups[$stop] instanceof Collection ){
            $item = $this->groups[$stop]->get($carrier, []);
        }
        return $item;
    }


    public function getBaseGrid(){
        // Build column header array (i.e. array of airlines)
        foreach($this->uniqueCarriers as $carrier){
           
            $this->baseGrid2[0][] = [
                'label' => $carrier,
                'field' => 'airline',
                'type' => 'colHeader',
                'image' => 'xxx',
                'queryParams' =>[
                    'value' => $carrier,
                ]
            ];
        }


        foreach($this->baseGrid2[0] as $key => $value){

            if($key > 0) {
                $_carrier = $value['label']; //mu

                $i = 1;
                $max = 3;


                while ($i <= $max) {


                    $_stop = $this->baseGrid2[$i][0]['queryParams']['stop'];

                    $tmp = $this->getItemByStopCarrier($_stop, $_carrier);
                    $cellValues = [
                        'type'=>'blank'
                    ];
                    if(!empty($tmp)){
                        $cellValues = [
                            //'row' => $rowKey+1,
                            // 'column' =>$colKey+1,
                            'stop' => $_stop,
                            'carrier' => $_carrier,
                            'type' => 'content',
                            'queryParams' => [
                                'stop' => $_stop,
                                'carrier' => $_carrier,

                            ]
                        ];


                        $cellValues  = array_merge($cellValues,$tmp );
                    }

                    $this->baseGrid2[$i][] = $cellValues;

                    $i++;

                }
            }

        }
       // print_r($this->baseGrid2);
      
        return $this->baseGrid2;
    }

    public function runMe(Store $store){
        $pnr = new GundamModels();
        $pnr->ping();
        dd(1);
        // var_dump($store);

        // var_dump(Session::get('store'));

        // $x = ['a'=>1];

        // $f = new Flights();

        // $f->search();
        // $c = new Cars();
        // $c->search();

        // dd(1);

        // echo array_get($x,'b');
       
        $this->uniqueCarriers = $this->getUniqueCarriers($this->x);
      //  print_r($this->x);
        $c = collect($this->x);

        $groups = [
            0 => [],
            1 => [],
            2 => [],
        ];

        $groups[0] = $c->filter(
                function($item, $key){
                    return (count($item['seg']) == 1 );
                }
            );

        $groups[1] = $c->filter(
            function($item, $key){
                return (count($item['seg']) == 2 );
            }
        );

        $groups[2] = $c->filter(
            function($item, $key){
                return (count($item['seg']) > 2 );
            }
        );

        foreach($groups as $k => $g){
            $carriers = $g->sortBy('price')->groupBy('carrier');

            foreach($carriers as $kc => $value){

                $carriers[$kc] = $value->first();
            }

            $groups[$k] = $carriers;
        }

        $this->groups = $groups;
        $this->lookupResults = $groups;

        $baseGrid = $this->getBaseGrid();
        //dd($baseGrid);
        //return $this->groups;
        return view('index', ['baseGrid'=>$baseGrid]);
    }
}
