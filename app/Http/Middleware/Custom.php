<?php
namespace App\Http\Middleware;
use Closure;
use App\Libraries\Store;
use Session;

class Custom{
    public function handle($request, Closure $next){
      
        $store = new Store();
        $store->name='cat';
        Session::put('store', $store);
        app()->bind('App\Libraries\Store', function() use($store){
            return $store;
        });
      
        return $next($request);
    }
}