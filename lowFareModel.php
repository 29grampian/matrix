<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
class FilterManager{
    public function filter($request, $data){
        if(isset($request['filter']['name'])){
             $data['itineraries'] = array_filter(
                $data['itineraries'], 
                function($item) use($request){
                return $item['name'] === $request['filter']['name'];
                });
        };
        return $data;
    }
}
class FilterDisplayDataManager{
    public function get(LowFareModel $lf)
    {
       $items = array_map(
           function($item){
               return $item['name'];
            }, 
            $lf->getItineraries() // no need to check for index, the Model returns safe data
        );
        return array_unique($items);
    }
}
class AbstractFlightsModel{
    protected $response;
    protected $request;
    public function all()
    {
        return $this->response;
    }

    public function setRequest($request)
    {
        $this->request = $request;
        return $this;
    }
    public function setResponse($response){
        $this->response = $response;
        return $this;
    }
}

class lowFareModel extends AbstractFlightsModel
{
    private $filterManager;
    
    public function __construct()
    {
        $this->filterManager = new FilterManager();
    }
    
    public function get(array $request)
    {
        $this->request = $request;
        // call FlightbookingEngine.php to call data
        $this->response = [
          'segments' => [
              'a' => ['name'=>'a'],
          ],
          'itineraries' => [
                ['name'=>'z'],
                ['name'=>'a'],
                ['name'=>'b'],
                ['name'=>'b'],
                ['name'=>'c'],
          ],
          
        ];
        return $this;
    }
   
    public function getSegments(){
        // internal safety check, returns default
        return (array_key_exists('segments', $this->response)) ? $this->response['segments'] : [];
    }
    
    public function getSegmentByIdSingle(string $id, array $segment){

    }
    
    public function getItineraries(){
        // internal safety check, returns default
        return (array_key_exists('itineraries', $this->response)) ? $this->response['itineraries'] : [];
    }

    public function filter()
    {
        if($this->isValidResponse()){
            $this->response = $this->filterManager->filter($this->request, $this->response);
        }
        
        return $this; // return this so we can chain
    }

    public function sort()
    {
        if($this->isValidResponse()){
            $iti = $this->getItineraries();
            // will call sort manager
            if(isset($this->request['sort']) && $this->request['sort'] == 'name_asc'){
                uasort($iti, function($a, $b){
                    return $a['name'] <=> $b['name'];
                });
                $this->response['itineraries'] = $iti;
            }
        }
        return $this;
    }

    public function paginate(){
        if($this->isValidResponse()){
            if(isset($this->request['stop'])){
                $this->response['itineraries'] = array_slice($this->getItineraries(), 0, $this->request['stop']);
            }
        }
        return $this;
    }
    public function isValidResponse(){
        $keys = ['itineraries', 'segments'];
        foreach($keys as $key)
        {
            if( !isset($this->response[$key]) || !is_array($this->response[$key]) ){
               return false;
            }    
        }

        return true;
    }
}

// You are in controller now
session_start();

$lowFareModel = new lowFareModel();

// 1 basic get all
$r1 = $lowFareModel->get(['param'=>'something'])->all();
echo "1. basic==========\n";
print_r($r1);

$r1 = $lowFareModel->get(['stop'=>2])->paginate()->all();
echo "1.1. basic paginate==========\n";
print_r($r1);

$r1 = $lowFareModel->get(['stop'=>2,'sort'=>'name_asc'])->sort()->paginate()->all();
echo "1.2. basic sort paginate==========\n";
print_r($r1);

// 2. chain filter sort
$r2 = $lowFareModel->get(['filter'=>['name'=>'b'],'sort'=>'name_asc'])->filter()->sort()->all();
echo "2. chain filter sort==========\n";
print_r($r2);

// 3. postFilteredSearch Filter overload with session data
$lowFareModel = new lowFareModel();
$r1 = $lowFareModel->get(['param'=>'something'])->all();
$_SESSION['response'] = $r1;
$r3 = $lowFareModel
->setRequest(['filter'=>['name'=>'a']])
->setResponse($_SESSION['response'])
->filter()
->sort()
->paginate()
->all();
echo "3. overload with session data==========\n";
print_r($r3);


$lowFareModel = new lowFareModel();
$lowFareModel->get(['param'=>'something'])->all();
$filterDisplayDataManager = new FilterDisplayDataManager();
$filterDisplayData = $filterDisplayDataManager->get($lowFareModel);
echo "4.Pass Model to someone else to extract data==========\n";
print_r($filterDisplayData);

/*
To avoid doing too much of this in controller:
$results = $f->get();
$results = $anotherClass->filter($results);
$results = $anotherClass2->sort($results);
$results = paginateMe($results);
*/