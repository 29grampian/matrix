import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import styled from 'styled-components'
class renderHelper {
    constructor(params){
        this.params = params;
        this.typeToClass = {
            'colHeader':'ch',
            'rowHeader':'rh',
        }
    }
    get className(){
        return (this.params.type in this.typeToClass) ?
            this.typeToClass[this.params.type] : 'default';
    }
}

const StyledCell = styled.div`
font-weight:bold;
border:1px solid red;
`;

class Cell extends Component{
    constructor(props){
		super(props);
		this.renderHelper = new renderHelper(props.params);
    }
    render(){
        return (
            <StyledCell className={this.renderHelper.className}>cell</StyledCell>
        );
    }
}
const StyledRow = styled.div`
    border:1px solid green;
    color:green;
`;
class Row extends Component{
    renderCells() {
        return this.props.cells.map(
            (cell, i) => <Cell key={i} params={cell}/>
        )
    }
    render(){
        return (
            <StyledRow>
                {
                    this.renderCells()
                }
            </StyledRow>
        )
    }
}

class MatrixContainer extends Component{
    constructor(props){
		super(props);
		this.state = {
            matrix: data.matrix,
            a: data.a
		}
    }
    renderRows() {
        return this.state.matrix.map(
            (row, i) => <Row key={i} cells={row}/>
        )
    }
    render(){
        return (
            <div>
               {this.renderRows()}
            </div>
        );
    }
}

ReactDOM.render(
    <MatrixContainer/>,
    document.querySelector("#app")
)
