console.log(VERSION);
import React, { useState } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
// class a1 extends React.Component{
//     render(){return<p>{this.props.name}</p>}
// }
// class a2 extends React.Component{
//     render(){return<p>{this.props.name}</p>}
// }

// let App = ("noRapid" == VERSION) ? a1 : a2;

sessionStorage.setItem('queue', 'flights,hotels,cars');

const onClick = e => {
    e.preventDefault();
    
    let flow = sessionStorage.getItem('queue');
    let parts = flow.split(',')
    let product = parts.shift();
    let flowDeQ = parts.join(',')
    sessionStorage.setItem('queue', flowDeQ);

    document.myForm.action = `/${product}`;
    document.myForm.queueSafe.value=flowDeQ;
   
    document.myForm.submit();
  

}
const onRadioChange = (v, callback) => {
    console.log('change', v);
    sessionStorage.setItem('queue', v);
    callback(v);
}
function App(props){
    const [radio, setRadio] = useState("flights,hotels,cars");
    const p = [
        'flights,hotels,cars',
        'flights,hotels',
        'flights,cars'
    ];
    return(
        <div>
        <p>Current: {radio}</p>
        <form onSubmit={e=>onClick(e)} name="myForm" method="POST">
        <input type="hidden" name="queueSafe" value={radio}/>
            {
                p.map( (i, index) => {
                   
                    return (
                        <label key={index}>{i} 
                        <input
                      
                        type="radio" 
                        name="queue"
                        value={i}
                        defaultChecked={0===index?true:false}
                        onChange={()=>onRadioChange(i, setRadio)}
                        />
                        </label>
                    )

                })
            }
          
            <input type="hidden" name="_token" id="csrf-token" value={csrf_token}/>
           <input type="submit" value="Search"  />
        </form>
        </div>
    );
}
ReactDOM.render(
    <App name={VERSION}/>,
    document.querySelector("#abc")
);