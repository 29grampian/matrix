@extends('base')
@section('content')
some form<br/>


<script>
  var _token = '{{csrf_token()}}';
</script>

Test 1 Submit
<form method="post" action="test">
  @csrf
  <input type="submit" value="submit"/>
</form>
<br/>




Test 2 Submit use js 
<script>
function submit1(event){
  event.preventDefault();
  confirm('yes?');
  document.getElementById('form2').submit();
}
</script>
<form method="post" action="test" id="form2">
  @csrf
  <input type="submit" value="submit" onClick="submit1(event)"/>
</form>
<br/>



Test 3 ajax + fetch + formData (most modern)<br/>
<script>
  function submit3(event){

    var data = new FormData();
    data.append('_token', _token);
    //https://muffinman.io/uploading-files-using-fetch-multipart-form-data/
    fetch('/test',{
      method: 'post',
     
      body: data
    }).then(res=>{
      console.log(res)
    })
  }
</script>
<button onClick="submit3(event)">Submit</button><br/>
<br/>



Test 4 xhr + formData<br/>
<script>
  function submit4(event){
    confirm('f4');
    var f = document.getElementById('f4');
    var fd = new FormData(f);
    fd.append('_token', _token);
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(){
      if(this.readyState == 4 && this.status == 200){
        console.log(this.responseTest);
      }
    }
    xhr.open("POST", "/test", true);
    xhr.send(fd)
  }
</script>
<form id="f4">
<input type="text" value="bar" name="foo"/>
<input type="text" value="john" name="traveler[0][name]"/>
<input type="text" value="12" name="traveler[0][age]"/>
</form>
<button onClick="submit4(event)">Submit</button><br/>

<br/>

Test 5 using formdata<br/>

<script>
  function submit5(event){
    event.preventDefault();
    var myform = document.createElement('form');
    myform.method='post';
    myform.action='/test';
    myform.id="form3";

    myinput = document.createElement('input');
    myinput.type='text';
    myinput.name='_token';
    myinput.value=_token;
    myform.appendChild(myinput);
   
    var data = new FormData(myform);
    data.append('foo', 'bar');
    document.body.append(myform);

    document.getElementById('form3').submit();
 
  }
</script>


<button onClick="submit5(event)">Submit</button>
<div id="myForm"></div>

@endsection