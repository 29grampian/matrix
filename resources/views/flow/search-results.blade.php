@extends('flow.base')

@section('content')
    <script>
         sessionStorage.setItem('queue', '{{$queue}}');
    </script>
    <h1>At {{$product}}</h1>
    <h2>Incoming Queue {{$queue}}</h2>
    <h2>Next step {{$url}}</h2>
    <h2>Cart</h2>
    <?php
        print_r($cart);
    ?>
    <form method="post" action="{{$url}}">
        @csrf
        <input type="submit" value="submit"/>
    </form>
@endsection