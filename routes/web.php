<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'MyController@runMe')->middleware(['custom']);



Route::get('/flights/review', 'FlightsController@getReview');
Route::post('/flights/review', 'FlightsController@postReview');

Route::get('/test', 'TestMeController@getIndex');
Route::post('/test', 'TestMeController@postIndex');
Route::get('/travel', 'FlowController@getIndex');


Route::post('/travel', 'FlowController@postIndex');



Route::post('/{product}', 'FlowController@postProductSearch');
Route::get('/{product}', 'FlowController@getProductSearch');


Route::post('/{product}/finish', 'FlowController@postFinish');